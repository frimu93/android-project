## Documentation d'utilisation

*  Quand vous démarrez l'application vous avez 2 possibilités

<img src="images/MenuActivity.png" width="200">

* Soit vous créez un nouveau monstre
* Soir vous chargez un monstre déjà disponible
* Si vous créez un monstre:
	* Vous commencez par renseigner le nom du monstre: 
	
	<img src="images/CreateMonsterActivity.png" width="200">
	
	* Puis vous définissez des émotions:
	
	<img src="images/CreateEmotionsActivity.jpg" width="200">
	
	* Une fois une émotion défini vous pouvez la valider ou en revenir en arrière:
	
	<img src="images/ValidateEmotionActivity.jpg" width="200">
	
* Une fois créé ou chargé vous arrivez sur la page d’accueil du monstre

	<img src="images/HomeActivity.jpg" width="200">
	
* Sur cette page vous avez plusieurs boutons:
	* un bouton nourrir qui va nourrir le monstre
	* un bouton jouer qui ouvre une page où on peut jouer avec le monstre
	
	<img src="images/FetchBall.jpg" width="200">
	
	* Un bouton monstre qui ouvre la page de chargement de monstres
	
	<img src="images/LoadMonsterActivity.jpg" width="200">
	
* Vous avez aussi plusieurs actions:
	* faire bouger le téléphone fait bouger le monstre
	* Taper sur le monstre provoque une action
	
* Si vous créez un monstre mais que vous ne le nourrissez pas il va mourir au bout d'un certain temps
* Une fois mort vous ne pouvez plus faire aucune action avec votre monstre

<img src="images/dead.jpg" width="200">



## Architecture du projet

<img src="images/architecture.png" >

## Organisation du travail
Chacun a eu plusieurs issues git à faire

## Difficultés rencontrés
* Difficulté de converger le résultat final
* Difficulté de choisir quoi utiliser pour la création de l'image, manipulation d'images, camera

## Choix techniques
* On a décidé d'utiliser l'api camera2 parce que l'api camera est maintenant deprecated et on ne pouvait pas la faire marcher sur nos portable android 6

## License: 
```
Creative Commons
```
## Credits:
```

audio composer jorickhoofd
https://www.freesound.org/people/jorickhoofd/
```