package fr.umlv.facemonsters;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Notification.setNotification(this);
        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);


    }

    public void createMonster(View view){
        startActivity(new Intent(this, CreateMonsterActivity.class));
    }

    public void loadMonster(View view){
        startActivity(new Intent(this, LoadMonsterActivity.class));
    }

    @Override
    public void onPause(){
        super.onPause();

        Intent svc = new Intent(this, BackgroundMusic.class);
        stopService(svc);

    }

    @Override
    public void onResume(){
        super.onResume();

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

    }



}
