package fr.umlv.facemonsters;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import java.util.Calendar;
import java.util.Date;



/**
 * Created by stefan on 26/03/17.
 */

public class Notification {

    public static void setNotification(Activity activity){
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 40);
        calendar.set(Calendar.SECOND, 0);
        if(calendar.getTime().compareTo(new Date()) < 0)
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        Intent intent1 = new Intent(activity, AlarmReciever.class);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 0,intent1, PendingIntent.FLAG_UPDATE_CURRENT);


        AlarmManager am = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}