package fr.umlv.facemonsters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.Image;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by stefan on 24/03/17.
 */

public class FaceMonster implements Serializable {
    private HashMap<String, BitmapDataObject> map = new HashMap<>();
    private String name;
    private int health = 100;
    private boolean isAlive = true;

    public void setName(String name){
        this.name = Objects.requireNonNull(name);
    }

    public String getName(){
        return name;
    }
    public int getHealth() { return health; }
    public boolean isAlive() { return isAlive; }

    public FaceMonster(){

    }

    public void set(String action, Bitmap bitmapImage, Bitmap mask){

        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 540, 960, false);
        Bitmap maskScaled = Bitmap.createScaledBitmap(mask, 540, 960, false);

        Bitmap fnl = Bitmap.createBitmap(scaled, 130, 170, 280, 450);
        Bitmap fnlMaks = Bitmap.createBitmap(maskScaled, 130, 170, 280, 450);

        applyMask(fnl, fnlMaks);

        fnl.setHasAlpha(true);
        map.put(action, new BitmapDataObject(fnl));
    }

    public Bitmap get(String action){
        return map.get(action).getBitmap();
    }

    void saveToDisk(Context context, String path) throws IOException {
        FileOutputStream fos = context.openFileOutput(path, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(map);
        os.writeUTF(name);
        os.writeInt(health);
        os.writeBoolean(isAlive);
        os.close();
        fos.close();
    }

    void loadFromDisk(Context context, String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(path);

        ObjectInputStream is = new ObjectInputStream(fis);
        map.clear();

        HashMap<String, BitmapDataObject> simpleClass = (HashMap<String, BitmapDataObject>) is.readObject();
        String monsterName = is.readUTF();
        int health = is.readInt();
        boolean isAlive = is.readBoolean();
        is.close();
        fis.close();
        map = simpleClass;
        name = monsterName;
        this.health = health;
        this.isAlive = isAlive;
    }

    private void applyMask(Bitmap src, Bitmap mask){
        for (int x = 0; x < src.getWidth(); x++) {
            for (int y = 0; y < src.getHeight(); y++) {
                int maskPixel = mask.getPixel(x, y);
                if(Color.alpha(maskPixel) == 0){
                    src.setPixel(x, y, Color.TRANSPARENT);
                }
            }
        }
    }

    /**
     * Feed the monster.
     */
    public void feed() {
        health += 20;
        if (health > 100) health = 100;
    }

    /**
     * The monster is hungry, its health decreases.
     */
    public void hungers() {
        health -= 5;
        if (health <= 0) {
            health = 0;
            this.dies();
        }
    }

    /**
     * The monster dies.
     */
    public void dies() {
        isAlive = false;
    }
}
