package fr.umlv.facemonsters;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.params.Face;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;

/**
 * Created by stefan on 26/03/17.
 */

public class AlarmReciever extends BroadcastReceiver {
    private int nId = 0;
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent resultIntent = new Intent(context, MenuActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                context,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );



        long when = System.currentTimeMillis();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_user_ball)
                        .setContentTitle("Face Monsters")
                        .setContentIntent(resultPendingIntent)
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setVibrate(new long[]{1000, 1000}).setWhen(when)
                        .setContentText(context.getString(R.string.monster_play));

        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(nId,mBuilder.build());
        nId++;
    }
}
