package fr.umlv.facemonsters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class CreateMonsterActivity extends Activity {
    private String monsterName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_monster);

        EditText et = (EditText) findViewById(R.id.editMonsterName);
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    createEmotions(v);
                    return true;
                }
                return false;
            }
        });

    }

    protected void createEmotions(View view){
        Intent intent = new Intent(this,CreateEmotionsActivity.class);

        EditText et = (EditText) findViewById(R.id.editMonsterName);
        String text = et.getText().toString();

        intent.putExtra("name", text);

        startActivity(intent);
    }
}
