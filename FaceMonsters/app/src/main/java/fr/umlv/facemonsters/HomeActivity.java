package fr.umlv.facemonsters;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by Bryan on 26/03/2017.
 */

public class HomeActivity extends Activity implements SensorEventListener {
    private final FaceMonster monster = new FaceMonster();

    private ImageView image_monster;


    private MediaPlayer auwMp ;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private AnimationDrawable drawableAnimation;
    private Animation animBreath;


    /* fields for fetch ball*/
    private int xDelta, yDelta;
    private View ball;
    private ViewGroup mainLayout;
    int monsterCenterX;
    int monsterCenterY;
    int screenCenterX;
    int screenCenterY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        String path = null;

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);


        Intent intentGot = getIntent();
        if (intentGot != null) {
            Bundle extras = intentGot.getExtras();
            if (extras != null) {
                path = extras.getString("path");
                try {
                    monster.loadFromDisk(getBaseContext(), path);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

        initHealthBar(path);

                /* Buttons listeners */
        Button btnMonsters = (Button) findViewById(R.id.btnMonsters);
        btnMonsters.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent myIntent = new Intent(HomeActivity.this, LoadMonsterActivity.class);
                startActivity(myIntent);
            }
        });

        if (!monster.isAlive()) {
            this.image_monster = (ImageView) findViewById(R.id.monster_image);
            image_monster.setImageBitmap(monster.get("dying"));
            deadScenery();
            return;
        }

        animateIdle();



        Button btnFeed = (Button) findViewById(R.id.btnFeed);
        btnFeed.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                monster.feed();

                ProgressBar healthbar = (ProgressBar) findViewById(R.id.healthbar);
                TextView textViewHealth = (TextView) findViewById(R.id.health);

                textViewHealth.setText(String.valueOf(monster.getHealth())); // update text
                healthbar.setProgress(monster.getHealth()); // update bar
            }
        });

        setPainAnimation();

        Button btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, FetchBall.class);
                intent.putExtra("path", monster.getName());
                startActivity(intent);
            }
        });



    }

    /**
     * Animate the monster with an idle animation
     */
    private void animateIdle() {
        Bitmap idleBMP = monster.get("idle");
        drawableAnimation = new AnimationDrawable();
        if (idleBMP != null) {
            this.image_monster = (ImageView) findViewById(R.id.monster_image);
            //image.setImageBitmap(idleBMP);

            drawableAnimation.addFrame(new BitmapDrawable(getResources(), idleBMP), 2000);

            Bitmap sleepBMP = monster.get("sleep");
            if (sleepBMP != null) {
                drawableAnimation.addFrame(new BitmapDrawable(getResources(), sleepBMP), 100);
            }

            image_monster.setImageDrawable(drawableAnimation);

            final AnimationSet animationSet = new AnimationSet(false);//false means don't share interpolators
            animBreath = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.breath);
            final Animation bounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.happy);
            bounce.setStartOffset(1000);
            animationSet.addAnimation(animBreath);
            animationSet.addAnimation(bounce);

            animBreath.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.breath);

                    anim.setAnimationListener(this);
                    if(monster.isAlive())
                        image_monster.startAnimation(anim);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            bounce.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    image_monster.setImageBitmap(monster.get("happy"));
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    image_monster.setImageDrawable(drawableAnimation);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });

            image_monster.post(new Runnable() {
                @Override
                public void run() {
                    drawableAnimation.start();
                    image_monster.startAnimation(animationSet);
                }
            });
        }// end of animation idle
    }

    private void setPainAnimation(){
        final ImageView image = (ImageView) findViewById(R.id.monster_image);
        final Animation animPain = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.pain);
        animPain.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.d("anim", "started");
                if(auwMp != null && auwMp.isPlaying()) {
                    auwMp.stop();
                }
                auwMp = MediaPlayer.create(getBaseContext(), R.raw.auw);

                auwMp.seekTo(700 * ((int) (Math.random() * 4)));
                auwMp.start();
                image.setImageBitmap(monster.get("angry"));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(auwMp.isPlaying()) {
                    auwMp.stop();
                }
                image.clearAnimation();
                image.setImageBitmap(monster.get("idle"));
                image.setImageDrawable(drawableAnimation);
                image.startAnimation(animBreath);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if(monster.isAlive())
                            image.startAnimation(animPain);

                    }
                });

            }
        });

    }

    /**
     * Initialize the health bar of the monster
     * @param path
     */
    private void initHealthBar(final String path) {
        // Health points
        final TextView textViewHealth = (TextView) findViewById(R.id.health);
        Log.i("HomeActivity", "Name="+monster.getName()+" Health="+monster.getHealth());
        textViewHealth.setText(String.valueOf(monster.getHealth()));

        // Health bar
        final ProgressBar healthbar = (ProgressBar) findViewById(R.id.healthbar);
        healthbar.setProgress(monster.getHealth());

        /* Automatic decrease of the health points */
        final Handler healthHandler = new Handler();
        final int delay = 10000; //ms
        healthHandler.postDelayed(new Runnable(){
            public void run(){
                monster.hungers(); // decreases health
                textViewHealth.setText(String.valueOf(monster.getHealth())); // update text
                healthbar.setProgress(monster.getHealth()); // update bar
                if (!monster.isAlive()) { // if dead
                    image_monster.setImageBitmap(monster.get("dying")); // dying face
                    image_monster.clearAnimation(); // clear animations
                    try {
                        monster.saveToDisk(getBaseContext(), path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    deadScenery(); // disable all buttons but Monsters, background of death
                    return;
                }
                healthHandler.postDelayed(this, delay);
            }
        }, delay);
    }

    private void deadScenery() {
        // disable buttons, dead style
        Button btnFeed = (Button) findViewById(R.id.btnFeed);
        Button btnPlay = (Button) findViewById(R.id.btnPlay);
        Button btnShare = (Button) findViewById(R.id.btnShare);
        btnFeed.setBackground(getDrawable(R.drawable.border_disabled));
        btnFeed.setEnabled(false);
        btnPlay.setBackground(getDrawable(R.drawable.border_disabled));
        btnPlay.setEnabled(false);
        btnShare.setBackground(getDrawable(R.drawable.border_disabled));
        btnShare.setEnabled(false);

        // dead style background
        ImageView background = (ImageView) findViewById(R.id.background);
        background.setImageResource(R.drawable.background_dead);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float axisX = sensorEvent.values[0];
        if (Math.abs(axisX) > 8 && monster.isAlive()) {

            final ImageView image = (ImageView) findViewById(R.id.monster_image);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.shake);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    image.setImageBitmap(monster.get("dying"));
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    image.setImageBitmap(monster.get("idle"));
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });

            image.startAnimation(animation);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onPause(){
        super.onPause();

        Intent svc = new Intent(this, BackgroundMusic.class);
        stopService(svc);

    }

    @Override
    public void onResume(){
        super.onResume();

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

    }


}
