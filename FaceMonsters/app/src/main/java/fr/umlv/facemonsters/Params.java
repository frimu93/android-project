package fr.umlv.facemonsters;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLOutput;
import java.util.Set;

public class Params extends Activity {
    private BluetoothAdapter myBluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private ArrayAdapter<String> bluetoothArrayAdapter;
    private TextView paramMessage;
    private Button bluetoothBtn;
    private ListView listView;
    private boolean isActive = false;
    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("BLUETOOTH", "onReceive started");
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceInfos = device.getName() + "\n" + device.getAddress();
                bluetoothArrayAdapter.add(deviceInfos);
                Log.d("BLUETOOTH", "found: " + deviceInfos);
                bluetoothArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_params);

        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        paramMessage = (TextView) findViewById(R.id.params_message);
        if(myBluetoothAdapter == null) {
            paramMessage.setText(R.string.nobluetooth_support);
            Toast.makeText(getApplicationContext(), R.string.nobluetooth_support, Toast.LENGTH_LONG).show();
        } else {
            paramMessage.setText(R.string.bluetooth_off);
            bluetoothBtn = (Button) findViewById(R.id.bluetooth_btn);
            listView = (ListView) findViewById(R.id.list_devices);
            bluetoothArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
            listView.setAdapter(bluetoothArrayAdapter);

            bluetoothBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isActive = !isActive;
                    if(isActive) {
                        startBluetooth();
                        listDevices();
                        findDevices();
                        paramMessage.setText(R.string.bluetooth_on);
                    } else {

                        /*stopBluetooth();*/

                        paramMessage.setText(R.string.bluetooth_off);
                    }
                }
            });
        }
    }

    private void startBluetooth() {
        Log.d("BLUETOOTH", "start bluetooth");
        if(!myBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, 1);
            Toast.makeText(getApplicationContext(), R.string.bluetooth_on, Toast.LENGTH_LONG).show();
        }
    }

    private void stopBluetooth() {
        Log.d("BLUETOOTH", "stop bluetooth");
        myBluetoothAdapter.disable();
        Toast.makeText(getApplicationContext(), R.string.bluetooth_turnedoff, Toast.LENGTH_LONG).show();
    }

    private void listDevices() {
        pairedDevices = myBluetoothAdapter.getBondedDevices();
        for(BluetoothDevice device : pairedDevices) {
            bluetoothArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            Toast.makeText(getApplicationContext(),"Show Paired Devices", Toast.LENGTH_SHORT).show();
        }
    }

    private void findDevices() {
        bluetoothArrayAdapter.clear();
        myBluetoothAdapter.startDiscovery();
        registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bReceiver);
    }

}