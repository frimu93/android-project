package fr.umlv.facemonsters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

public class ValidateEmotionActivity extends Activity {
    private boolean isLast = false;
    private final FaceMonster monster = new FaceMonster();
    private String action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_emotion);

        Intent intentGot = getIntent();

        TextView tv = (TextView) findViewById(R.id.pose_message);

        if( intentGot != null) {
            Bundle extras = intentGot.getExtras();
            if (extras != null) {
                try {
                    monster.loadFromDisk(getBaseContext(),extras.getString("path"));
                } catch (IOException|ClassNotFoundException e) {

                    tv.setText("Load monster error");
                }
                Bitmap idleBMP = monster.get("idle");
                isLast = extras.getBoolean("isLast");
                action = extras.getString("action");
                tv.setText(tv.getText() + " ?");

                final AnimationDrawable drawableAnimation = new AnimationDrawable();
                if(idleBMP != null) {
                    final ImageView image = (ImageView) findViewById(R.id.monster_image);
                    //image.setImageBitmap(idleBMP);

                    drawableAnimation.addFrame(new BitmapDrawable(getResources(), idleBMP), 1000);

                    if (!action.equals("idle")) {
                        Bitmap secondBMP = monster.get(action);
                        if (secondBMP != null) {
                            drawableAnimation.addFrame(new BitmapDrawable(getResources(), secondBMP), 500);
                        }
                    }

                    image.setImageDrawable(drawableAnimation);

                    image.post(new Runnable() {
                        @Override
                        public void run() {
                            drawableAnimation.start();
                        }
                    });
                }
            }
        }


    }

    public void validate(View v){

        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void invalidate(View v){
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, resultIntent);
        finish();
    }
}
