package fr.umlv.facemonsters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class LoadMonsterActivity extends ActionBarActivity {
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_monster);

        listView = (ListView) findViewById(R.id.listView);

        ArrayList<FaceMonster> list = new ArrayList<>();


        File dirFiles = getBaseContext().getFilesDir();
        for(String file: dirFiles.list()){
            if(file.startsWith("wip_")) {
                continue;
            }
            FaceMonster monster = new FaceMonster();
            try {
                monster.loadFromDisk(getBaseContext(), file);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            monster.setName(file);
            Log.i("LoadFromDisk Monster", "Name="+monster.getName()+" Health="+monster.getHealth());
            list.add(monster);
        }

        final MonsterAdapter ma = new MonsterAdapter(this, list);
        listView.setAdapter(ma);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(LoadMonsterActivity.this,HomeActivity.class);

                intent.putExtra("path", ma.getMonster(position).getName());

                startActivity(intent);
            }
        });
        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

    }

    @Override
    public void onPause(){
        super.onPause();

        Intent svc = new Intent(this, BackgroundMusic.class);
        stopService(svc);

    }

    @Override
    public void onResume(){
        super.onResume();

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

    }


}
