package fr.umlv.facemonsters;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by Fuji on 26/03/2017.
 */

public class FetchBall extends Activity{
    private ViewGroup mainLayout;
    private View ball;
    private ImageView monster;

    private int screenCenterX, screenCenterY;
    private int monsterCenterX;
    private int monsterCenterY;
    private float ballCenterX, ballCenterY;
    private int xDelta;
    private int yDelta;
    private String path;
    private FaceMonster fmonster = new FaceMonster();

    private final Object lock1 = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetch_ball);
        mainLayout = (RelativeLayout) findViewById(R.id.fetchball_main);
        ball = findViewById(R.id.ball);
        monster = (ImageView) findViewById(R.id.monster);

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

        Intent intentGot = getIntent();

        try {
            fmonster.loadFromDisk(getBaseContext(), intentGot.getStringExtra("path"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Button back = (Button) findViewById(R.id.backBtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final Animation animBreath = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.breath);
        animBreath.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.breath);

                anim.setAnimationListener(this);
                if(fmonster.isAlive())
                    monster.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        final AnimationDrawable drawableAnimation = new AnimationDrawable();
        Bitmap idleBMP = fmonster.get("idle");
        drawableAnimation.addFrame(new BitmapDrawable(getResources(), idleBMP), 2000);

        Bitmap sleepBMP = fmonster.get("sleep");

        drawableAnimation.addFrame(new BitmapDrawable(getResources(), sleepBMP), 100);

        monster.setImageDrawable(drawableAnimation);

        monster.post(new Runnable() {
            @Override
            public void run() {
                drawableAnimation.start();
                monster.startAnimation(animBreath);
            }
        });


        //Centre du monstre
        monsterCenterX = monster.getWidth()/2;
        monsterCenterY = monster.getHeight()/2;

        //Centre de l'ecran
        screenCenterX = getResources().getDisplayMetrics().widthPixels/2;
        screenCenterY = getResources().getDisplayMetrics().heightPixels/2;



        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(200,200);
        layoutParams.topMargin = (getResources().getDisplayMetrics().heightPixels/4)*3;
        layoutParams.leftMargin = getResources().getDisplayMetrics().widthPixels / 2 - 100;
        ball.setLayoutParams(layoutParams);
        ball.setOnTouchListener(onTouchListener());


        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(200,200);
        layoutParams2.topMargin = getResources().getDisplayMetrics().heightPixels/2;
        layoutParams2.leftMargin = getResources().getDisplayMetrics().widthPixels/2;

        monster.setLayoutParams(layoutParams2);
    }

    private OnTouchListener onTouchListener() {
        return new OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)
                                view.getLayoutParams();

                        xDelta = x - lParams.leftMargin;
                        yDelta = y - lParams.topMargin;
                        break;

                    case MotionEvent.ACTION_UP:
                        this.monster_fetch_ball(ball);
                        Log.d("ActionUP","ActionUP");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                .getLayoutParams();
                        layoutParams.leftMargin = x - xDelta;
                        layoutParams.topMargin = y - yDelta;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        view.setLayoutParams(layoutParams);
                        break;
                }
                mainLayout.invalidate();
                return true;
            }


            private void monster_fetch_ball(View ball) {
                ballCenterX = ball.getWidth()/2 + ball.getX();
                ballCenterY = ball.getHeight()/2 + ball.getY();

                //Centre de la balle à l'écran
                final View monster = findViewById(R.id.monster);
                Log.d("AnimateMonster1",ball.getLeft() + "  " + ball.getTop() + "   " + ballCenterX +"   "+ ballCenterY +"   "  + monsterCenterX + "   " + monsterCenterY );
                monster.animate().x(ballCenterX - monsterCenterX).y(ballCenterY - monsterCenterY - 100).setDuration(700).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fetch_ball();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                }).start();
            }

            private void fetch_ball() {
                int depart_x = getResources().getDisplayMetrics().widthPixels / 2 - 100;
                int depart_y = getResources().getDisplayMetrics().heightPixels / 4 * 3;
                Log.d("AnimateMonster2",depart_x +"   "+ depart_y+"   "  + monsterCenterX + "   " + monsterCenterY );
                monster.animate().x(depart_x - monsterCenterX).y(depart_y - monsterCenterY - 100).setDuration(1000).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        goCenter();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                ball.animate().x(depart_x).y(depart_y).setDuration(1000);
                return;
            }

            private void goCenter(){

                monster.animate().x(screenCenterX-monsterCenterX).y(screenCenterY-monsterCenterY).setDuration(1000).setListener(null);
            }
        };
    }

    @Override
    public void onPause(){
        super.onPause();

        Intent svc = new Intent(this, BackgroundMusic.class);
        stopService(svc);

    }

    @Override
    public void onResume(){
        super.onResume();

        Intent svc = new Intent(this, BackgroundMusic.class);
        startService(svc);

    }
}
