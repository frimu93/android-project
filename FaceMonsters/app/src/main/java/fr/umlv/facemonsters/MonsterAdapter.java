package fr.umlv.facemonsters;

import android.content.Context;
import android.hardware.camera2.params.Face;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by stefan on 24/03/17.
 */

public class MonsterAdapter extends BaseAdapter {
    // Une liste de personnes
    private List<FaceMonster> monsters;

    //Le contexte dans lequel est présent notre adapter
    private Context mContext;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater mInflater;

    public MonsterAdapter(Context context, List<FaceMonster> monsters) {
        mContext = context;
        this.monsters = monsters;
        mInflater = LayoutInflater.from(mContext);
    }


    @Override
    public int getCount() {
        return monsters.size();
    }

    @Override
    public Object getItem(int position) {
        return monsters.get(position).get("idle");
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem;

        if (convertView == null) {

            layoutItem = (LinearLayout) mInflater.inflate(R.layout.monster_list_item_layout, parent, false);
        } else {
            layoutItem = (LinearLayout) convertView;
        }


        TextView tv = (TextView)layoutItem.findViewById(R.id.monster_name);
        ImageView iv = (ImageView) layoutItem.findViewById(R.id.monster_image);

        FaceMonster monster = monsters.get(position);
        if(!monster.isAlive())
            tv.setText(monster.getName() + " (" + layoutItem.getResources().getString(R.string.dead) + ")");
        else
            tv.setText(monster.getName());

        if(monster.isAlive())
            iv.setImageBitmap(monster.get("idle"));
        else
            iv.setImageBitmap(monster.get("dying"));


        //On retourne l'item créé.
        return layoutItem;
    }



    public FaceMonster getMonster(int position) { return monsters.get(position);}
}
