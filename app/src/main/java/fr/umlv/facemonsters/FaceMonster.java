package fr.umlv.facemonsters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.Image;
import android.util.DisplayMetrics;
import android.view.Display;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by stefan on 24/03/17.
 */

public class FaceMonster implements Serializable {
    private HashMap<String, BitmapDataObject> map = new HashMap<>();
    private String name;

    public void setName(String name){
        this.name = Objects.requireNonNull(name);
    }

    public String getName(){
        return name;
    }

    public FaceMonster(){

    }

    public void set(String action, Image img, Bitmap mask){
        Matrix m = new Matrix();
        m.preScale(-1, 1);

        ByteBuffer buffer = img.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.capacity()];
        buffer.get(bytes);
        Bitmap bitmapImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);

        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 540, 960, false);
        Bitmap maskScaled =Bitmap.createScaledBitmap(mask, 540, 960, false);

        applyMask(scaled, maskScaled);

        Bitmap fnl = Bitmap.createBitmap(scaled, 130, 170, 280, 450);

        Bitmap fnlHorizontalMirrored = Bitmap.createBitmap(fnl, 0, 0, fnl.getWidth(), fnl.getHeight(), m, false);

        fnlHorizontalMirrored.setHasAlpha(true);
        map.put(action, new BitmapDataObject(fnlHorizontalMirrored));
        img.close();
    }

    public Bitmap get(String action){
        return map.get(action).getBitmap();
    }

    void saveToDisk(Context context, String path) throws IOException {
        FileOutputStream fos = context.openFileOutput(path, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(map);
        /*os.writeUTF(name);*/
        os.close();
        fos.close();
    }

    void loadFromDisk(Context context, String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(path);

        ObjectInputStream is = new ObjectInputStream(fis);
        map.clear();

        HashMap<String, BitmapDataObject> simpleClass = (HashMap<String, BitmapDataObject>) is.readObject();
        /*String monsterName = is.readUTF();*/
        is.close();
        fis.close();
        map = simpleClass;
        /*name = monsterName;*/
    }

    private void applyMask(Bitmap src, Bitmap mask){
        for (int x = 0; x < src.getWidth(); x++) {
            for (int y = 0; y < src.getHeight(); y++) {
                int maskPixel = mask.getPixel(x, y);
                if(Color.alpha(maskPixel) == 0){
                    src.setPixel(x, y, Color.TRANSPARENT);
                }
            }
        }
    }


}
